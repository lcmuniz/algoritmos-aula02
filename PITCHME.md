# Algoritmos e Linguagens de Programação

Aula 02 - Conceitos iniciais

Prof. Luiz Carlos Melo Muniz

lcmuniz@gmail.com

---

### Lógica de programação

- **Lógica de programação** é a técnica de se encadear raciocinios lógicos para atingir determinado objetivo.

---

### Algoritmo

- **Algoritmo** é uma sequência de passos finitos e ordenados com o objetivo de solucionar um problema. 

![Algoritmo](img1.png)

---

### Fritar um ovo

```python
algoritmo "Fritar um ovo"
   Pegar frigideira, ovo, óleo e sal
   Colocar óleo na frigideira
   Acender o fogo
   Colocar a frigideira no fogo
   Esperar o óleo esquentar
   Colocar o ovo
   Retirar quando pronto 
```

---

### Instrução

Indica a um computador uma ação elementar a ser executada. 

---

### Trocar uma lâmpada

```python
algoritmo "Trocar uma lâmpada"
   Se (lâmpada estiver fora de alcance)
      Pegar a escada
   Pegar a lâmpada
   Se (lâmpada estiver quente)
      Pegar pano
   Tirar lâmpada queimada
   Colocar lâmpada boa
```

---

### Fazer prova

```python
algoritmo "Fazer prova"
   Ler a prova
   Pegar a caneta
   Enquanto ((houver questão em branco) e (tempo não terminou)) faça
      Se (souber a questão)
         Resolvê-la
      Senão
         Pular para outra
   Entregar a prova
```
---

### Jogar jogo da velha

```python
algoritmo "Jogar jogo da velha"
   Enquanto ((existir um quadrado livre ) e (ninguém perdeu(ganhou) o jogo)) faça
      Espere a jogada do adversário, continue depois
      Se (existir um quadrado livre)
         Se (centro livre)
            Jogue no centro
         Senão
            Se (adversário tem 2 quadrados em linha com o terceiro desocupado)
               Jogue neste quadrado desocupado
            Senão
               Se (há algum canto livre)
                  Jogue neste canto 
```
---

### Programa de computador

Algoritmo escrito numa linguagem de computador (Java, Python, C, Pascal, Fortran, Delphi, Cobol e outras).

---

### Variáveis

- Uma variável é um local na memória principal do computador.

- As linguagens de programação permitem dar um nome a este local na memória para facilitar o acesso.

- O conteúdo de uma variável pode ser de vários tipos: inteiro, real, caractere, lógico, texto, etc.

- O nome de uma variável não pode ser alterado mas seu conteúdo sim.

---

### Variáveis em Python

- Alguns tipos de conteúdos (tipos de dados) em python:

    - int (inteiro). Ex: 2, 4, -5, 0
    - float (real). Ex: 2.4, 6.0, -3.15343
    - str (texto ou string). Ex: 'João', "programação", 'FACAM'
    - bool (lógico ou booleano). Ex: True e False

---

### Regras para nomear variáveis em Python

- O primeiro caractere deve ser uma letra ou \_
- Os demais caracteres devem ser letras, números ou \_
- Letras maiúsculas são diferentes de minúsculas
- Não pode ser usada uma palavra reservada pela linguagem
    
---

### Expressões

- Aritméticas
- Relacionais
- Lógicas

---

### Expressões aritméticas

- Soma. Ex: A + B, 2 + 3
- Subtração. Ex: A - B, 2 - 3
- Multiplicação. Ex: val1 \* val2, idade \* 3, 3 \* 4
- Divisão: Ex: numerador / denominador, 6 / 2
- Exponenciação. Ex: valor \*\* 2, 2 \*\* 3
- Resto ou módulo: 5 % 2, 7 % 4, 8 % 2

---

### Expressões relacionais

![Expressões relacionais](tab1.png)

---

### Expressões aritméticas

- valor != total
- x == 1
- 7 > 6
- 10 < 9
- 1 <= y

---

### Expressões lógicas

- Conjunção (e). Ex: (3 < 4) and (8 < 10)
- Disjunção (ou). Ex: (idade < 18) or (idade > 65)
- Negação (não). Ex: not maior_de_idade

---

```
até_a_proxima = True
```